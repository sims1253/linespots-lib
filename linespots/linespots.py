"""
The Linespots tool will rank each line of code of a git project by the
probability of bugs occurring in that line in the future. For in depth
information about this go visit www.coala.io/linespots

The result can be used for code audition or an improved review process.
"""
from collections import defaultdict
from tqdm import tqdm
from typing import DefaultDict, List, Callable


import linespots.utils.git_utils as git
from linespots.utils.weighting_functions import (
    normalize_timestamp_to_commit_range,
    google_weighting_function,
)


def score_commit(
    file_dict: DefaultDict[str, List[float]],
    commit: git.Commit,
    oldest_commit_age: int,
    youngest_commit_age: int,
    weighting_function: Callable[[float, float], float],
    commit_index: int = None,
):

    if commit_index:
        relative_commit_age = normalize_timestamp_to_commit_range(
            commit_index, oldest_commit_age, youngest_commit_age
        )
    else:
        relative_commit_age = normalize_timestamp_to_commit_range(
            commit.date, oldest_commit_age, youngest_commit_age
        )

    # Each file in the diff is scored according to the file mode (new,
    # delete, rename or modify)
    for file_diff in commit.file_diffs:

        # Try to remove deleted files from the dict
        if file_diff.file_mode == "delete":
            try:
                del file_dict[file_diff.filename]
            except KeyError:
                pass

        # Ignore binary files
        elif file_diff.is_binary or file_diff.is_mode_change:
            pass

        # Ignore empty new files. Score non empty ones
        elif file_diff.file_mode == "new":
            if file_diff.hunks:
                line_scores = []
                file_dict[file_diff.filename] = score_file_diff(
                    file_diff,
                    relative_commit_age,
                    line_scores,
                    commit.is_fix,
                    weighting_function,
                )

        # Simply change the key of a renamed file, if the file is unchanged,
        # score and change key otherwise
        elif file_diff.file_mode == "rename":
            if file_diff.similarity_index == 100:
                try:
                    file_dict[file_diff.filename] = file_dict.pop(
                        file_diff.old_filename
                    )
                except KeyError:
                    pass
            else:
                try:
                    line_scores = file_dict.pop(file_diff.old_filename)
                except KeyError:
                    line_scores = []
                file_dict[file_diff.filename] = score_file_diff(
                    file_diff,
                    relative_commit_age,
                    line_scores,
                    commit.is_fix,
                    weighting_function,
                )

        # Simply score the file as a default
        else:
            file_dict[file_diff.filename] = score_file_diff(
                file_diff,
                relative_commit_age,
                file_dict[file_diff.filename],
                commit.is_fix,
                weighting_function,
            )


def score_file_diff(
    file_diff: git.FileDiff,
    commit_age: float,
    old_line_scores: List[float],
    fix: bool,
    weighting_function: Callable[[float, float], float],
) -> List[float]:
    """
    Returns the line score list of a file after applying the file_diff to it.
    If the old file score list is empty, the function will return a list filled
    with a zero for each line in the new file.

    :param file_diff:
    :param commit_age: The normalized age of the commit the file diff is part of
    :param old_line_scores: The list of line scores before the commit is applied
    :param fix: True if the commit was a bug fix.
    :param weighting_function:
    :return: The list of line scores after the commit was applied
    """

    new_line_scores = []
    old_lines_index = 0
    # For each hunk in the diff, add the score list resulting of that hunk to
    # the new_line_scores. Also adds the untouched scores from the old
    # score list.
    for hunk in file_diff.hunks:

        # Fills up the new scores list up to the current hunk's start
        if hunk.old_start > old_lines_index:
            extension = old_line_scores[old_lines_index : hunk.old_start - 1]

            # If old_line_scores is shorter and doesn't offer values, we have to
            # add zeroes instead
            if len(extension) < (hunk.old_start - old_lines_index - 1):
                extension.extend(
                    [0.0] * ((hunk.old_start - old_lines_index - 1) - len(extension))
                )

            new_line_scores.extend(extension)
            old_lines_index = hunk.old_start - 1

        # Add the scores for the current hunk to the new scores list
        new_line_scores.extend(
            score_hunk_diff(hunk, commit_age, old_line_scores, fix, weighting_function)
        )

        # Keep track of where we are on the old scores list
        old_lines_index += hunk.old_length

    # If the old score list wasn't completely traversed by the hunks, the
    # missing lines are appended at the end of the new list unchanged.
    if old_lines_index < len(old_line_scores):
        new_line_scores.extend(old_line_scores[old_lines_index:])

    return new_line_scores


def score_hunk_diff(
    hunk: git.HunkDiff,
    commit_age: float,
    old_file_line_scores: list,
    fix: bool,
    weighting_function: Callable[[float, float], float],
) -> list:
    """
    Returns the line score list of a hunk.
    Untouched lines are taken from the old scores list, added lines are added
    with the average score of the old hunk lines plus a weight and removed
    lines' scores are dropped.

    :param hunk: List of lines from a diff hunk
    :param commit_age: The normalized age of the commit the file diff is part of
    :param old_file_line_scores: The list of line scores from the file
           containing this hunk, before the respective commit is applied
    :param fix: True if the commit was a bug fix.
    :param weighting_function: The weighting function that is used to calculate the
           score for added lines in case of a fix.
    :return: The list of line scores belonging to this hunk,
             after the hunk was applied
    """

    old_hunk_average = git.get_diff_hunk_average_score(
        old_file_line_scores, hunk.old_start - 1, hunk.old_length
    )
    new_line_scores = []
    old_line_nr = hunk.old_start
    # The first line contains metadata so we ignore it for diff parsing
    # The hunk is parsed
    for line in hunk.content:
        # '-' just increases the old line index
        if line.startswith("-"):
            old_line_nr += 1
        # '+' adds a new score
        elif line.startswith("+"):
            if fix:
                new_line_scores.append(weighting_function(commit_age, old_hunk_average))
            else:
                new_line_scores.append(0)
        else:
            # Ignore this as it is not part of the file but git output
            if line == "\ No newline at end of file":
                pass
            else:
                # Try to add the old score for this line if it exists. If not,
                # add the average hunk score. Then increase the old line index
                try:
                    new_line_scores.append(
                        weighting_function(
                            commit_age, old_file_line_scores[old_line_nr - 1]
                        )
                    )
                except IndexError:
                    new_line_scores.append(
                        weighting_function(commit_age, old_hunk_average)
                    )
            old_line_nr += 1
    return new_line_scores


def get_line_scores(
    commits: List[git.Commit],
    weighting_function: Callable[[float, float], float] = google_weighting_function,
    time_version: str = "time",
) -> DefaultDict[str, List]:
    """
    Returns a dictionary of line score lists with filenames as keys,
    calculated with the newest commit_depth commits.
    Uses fix_indicator to determine if a commit was a bug fix or not.
    Working directory must be a git repository for this to work.

    :param commits: List of commits to analyze
    :param commit_depth: The number of commits that will be used to calculate
                         the line scores
    :param fix_indicator: If this string is part of a commit message the commit
                          will be labeled as a bug fix
    :param weighting_function:
    :param time_version: Either 'time' or 'commit' for a time or for a commit based
                         time measurement.
    :return: A dictionary with a list of line scores for each file that was
             touched in the last commit_depth commits
    """

    # Get commits and determine the oldest and youngest commits for normalizing
    file_dict = defaultdict(list)
    if time_version == "time":
        oldest_commit_age = commits[-1].date
        youngest_commit_age = commits[0].date
    elif time_version == "commit":
        oldest_commit_age = 1
        youngest_commit_age = len(commits)
    else:
        raise ValueError(
            "time_version must either be 'time' or 'commit' but was {} instead!".format(
                time_version
            )
        )

    # Reverse so the oldest commit is scored first
    for commit_tuple in enumerate(
        tqdm(reversed(commits), desc="Scoring Commits", total=len(commits))
    ):
        if time_version == "time":
            score_commit(
                file_dict=file_dict,
                commit=commit_tuple[1],
                oldest_commit_age=oldest_commit_age,
                youngest_commit_age=youngest_commit_age,
                weighting_function=weighting_function,
            )
        elif time_version == "commit":
            score_commit(
                file_dict=file_dict,
                commit=commit_tuple[1],
                oldest_commit_age=oldest_commit_age,
                youngest_commit_age=youngest_commit_age,
                weighting_function=weighting_function,
                commit_index=commit_tuple[0] + 1,
            )

    return file_dict
