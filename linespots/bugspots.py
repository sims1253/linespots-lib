from collections import defaultdict
from tqdm import tqdm
from typing import DefaultDict, List, Callable


import linespots.utils.git_utils as git
from linespots.utils.weighting_functions import (
    normalize_timestamp_to_commit_range,
    google_weighting_function,
)


def score_commit(
    file_dict: DefaultDict[str, float],
    commit: git.Commit,
    oldest_commit_age: int,
    youngest_commit_age: int,
    weighting_function: Callable[[float, float], float],
    commit_index: int = None,
):

    if commit_index:
        relative_commit_age = normalize_timestamp_to_commit_range(
            commit_index, oldest_commit_age, youngest_commit_age
        )
    else:
        relative_commit_age = normalize_timestamp_to_commit_range(
            commit.date, oldest_commit_age, youngest_commit_age
        )

    # Each file in the diff is scored according to the file mode (new,
    # delete, rename or modify)
    for file_diff in commit.file_diffs:
        # Ignore binary files
        if file_diff.is_binary or file_diff.is_mode_change:
            pass

        # Ignore empty new files. Score non empty ones
        elif file_diff.file_mode == "new":
            file_dict[file_diff.filename] = 0

        # Try to remove deleted files from the dict
        elif file_diff.file_mode == "delete":
            try:
                del file_dict[file_diff.filename]
            except KeyError:
                pass

        # Simply change the key of a renamed file, if the file is unchanged,
        # score and change key otherwise
        elif file_diff.file_mode == "rename":
            try:
                file_dict[file_diff.filename] = file_dict.pop(file_diff.old_filename)
            except KeyError:
                pass

        # Simply score the file as a default
        else:
            if commit.is_fix:
                file_dict[file_diff.filename] = weighting_function(
                    file_dict[file_diff.filename], relative_commit_age
                )


def get_bugspots(
    commits: List[git.Commit],
    weighting_function: Callable[[float, float], float] = google_weighting_function,
    time_version: str = "time",
) -> DefaultDict[str, float]:

    file_dict = defaultdict(float)
    if time_version == "time":
        oldest_commit_age = commits[-1].date
        youngest_commit_age = commits[0].date
    elif time_version == "commit":
        oldest_commit_age = 1
        youngest_commit_age = len(commits)
    else:
        raise ValueError(
            "time_version must either be 'time' or 'commit' but was {} instead!".format(
                time_version
            )
        )

    # Reverse so the oldest commit is scored first
    for commit_tuple in enumerate(
        tqdm(reversed(commits), desc="Scoring Commits", total=len(commits))
    ):
        if time_version == "time":
            score_commit(
                file_dict=file_dict,
                commit=commit_tuple[1],
                oldest_commit_age=oldest_commit_age,
                youngest_commit_age=youngest_commit_age,
                weighting_function=weighting_function,
            )
        elif time_version == "commit":
            score_commit(
                file_dict=file_dict,
                commit=commit_tuple[1],
                oldest_commit_age=oldest_commit_age,
                youngest_commit_age=youngest_commit_age,
                weighting_function=weighting_function,
                commit_index=commit_tuple[0] + 1,
            )

    return file_dict
