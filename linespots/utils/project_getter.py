import os
from linespots.utils.git_utils import get_output


def run():
    project_urls = [
        "https://github.com/PrestaShop/prestashop.git",
        "https://github.com/scikit-learn/scikit-learn.git",
        "https://github.com/BroadleafCommerce/broadleafcommerce.git",
        "https://github.com/eclipse/ceylon-ide-eclipse.git",
        "https://github.com/woocommerce/woocommerce.git",
        "https://github.com/FFmpeg/ffmpeg.git",
        "https://github.com/rails/rails.git",
        "https://github.com/apache/lucene-solr.git",
        "https://github.com/MariaDB/server.git",
        "https://github.com/mysql/mysql-server.git",
        "https://github.com/mpc-hc/mpc-hc.git",
        "https://github.com/junit-team/junit5.git",
        "https://github.com/eclipse/rt.equinox.framework.git",
        "https://github.com/twbs/bootstrap.git",
        "https://github.com/python/cpython.git",
        "https://github.com/discourse/discourse",
        "https://github.com/Automattic/mongoose",
        "https://github.com/apache/commons-math.git",
        "https://github.com/google/closure-compiler.git",
        "https://github.com/jfree/jfreechart.git",
        "https://github.com/coala/coala.git",
        "https://github.com/GNOME/evolution.git",
        "https://github.com/apache/httpd.git",
    ]

    os.chdir("../../..")
    if not os.path.isdir("./evaluation_projects"):
        os.makedirs("evaluation_projects")
    os.chdir("evaluation_projects")

    for project in project_urls:
        get_output(["git", "clone", project])

    os.chdir("ceylon-ide-eclipse")
    get_output(["git", "checkout", "_old/master"])


if __name__ == "__main__":
    run()
