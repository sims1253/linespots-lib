import logging
from math import floor
import os
from subprocess import check_output, PIPE, Popen
import sys
from time import time
from tqdm import tqdm
from typing import List, Tuple, Dict, Pattern


class HunkDiff:
    def __init__(self, hunk: List[str]):
        self.old_start, self.old_length, self.new_start, self.new_length = get_hunk_metadata(
            hunk[0]
        )
        self.content = hunk[1:]


def get_hunks(file_diff: List[str]):
    return [HunkDiff(hunk) for hunk in get_chunks_by_line_start(file_diff, "@@ ", True)]


class FileDiff:
    def __init__(self, file_diff: List[str]):
        self.content = file_diff
        self.filename = file_diff[0].split()[3][2:]
        self.is_mode_change = any(
            (file_diff[1].startswith("old mode"), file_diff[2].startswith("new mode"))
        )
        self.is_binary = any(
            [diff.startswith("Binary files") for diff in file_diff[:7]]
        )

        if file_diff[1].startswith("new file mode"):
            self.file_mode = "new"
            # Test for empty file
            if len(file_diff) == 3 or self.is_binary or self.is_mode_change:
                self.hunks = None
            else:
                self.hunks = get_hunks(file_diff[5:])
        elif file_diff[1].startswith("deleted file mode"):
            self.file_mode = "delete"
        elif file_diff[2].startswith("rename from"):
            self.file_mode = "rename"
            self.old_filename = file_diff[2].split()[2]
            self.similarity_index = int(file_diff[1].split()[2][:-1])

            if self.similarity_index < 100:
                if not self.is_binary and not self.is_mode_change:
                    self.hunks = get_hunks(file_diff[7:])

        else:
            self.file_mode = "modify"
            if len(file_diff) == 3 or self.is_mode_change or self.is_binary:
                self.is_mode_change = True
                self.hunks = None
            else:
                self.hunks = get_hunks(file_diff[4:])


def get_file_diffs(diff: List[str]) -> List[FileDiff]:
    return [
        FileDiff(diff_chunk)
        for diff_chunk in get_chunks_by_line_start(diff, "diff --git")
    ]


def is_merge_commit(git_show: List[str]) -> bool:
    # The commit seperator is still in there
    if len(git_show[1].split()) >= 2:
        return True
    else:
        return False


def get_git_diff(sha: str) -> (List[str], List[str]):
    long_merge_diff = (
        get_output(["git", "diff", "{}^!".format(sha)]).strip("").splitlines()
    )
    assert not any(
        [True if not type(diff) is str else False for diff in long_merge_diff]
    )
    return long_merge_diff


class Commit:
    def __init__(self, sha: str, fix_indicator: Pattern = None):
        metadata = get_commit_metadata(sha)
        self.sha = sha
        self.date = metadata["timestamp"]
        self.parents = metadata["parents"]
        self.body = metadata["message"]
        self.diff = get_git_diff(self.sha)
        # I don't think it is needed anymore
        # self.is_merge = is_merge_commit(self.body)

        if fix_indicator:
            self.is_fix = is_fix_commit(self.body, fix_indicator)
        else:
            self.is_fix = None

        if self.diff:
            self.file_diffs = get_file_diffs(self.diff)
        else:
            self.file_diffs = []


def get_output(*args, **kwargs):
    """
    Retrieves the output from subprocess.check_output readily converted to a
    string. All arguments will be passed through.
    """
    output = check_output(*args, **kwargs).decode(errors="ignore")

    return output


def get_git_log(depth: int):
    hashes = get_output(
        ["git", "log", "-{}".format(str(depth)), "--first-parent", "--pretty='%H'"]
    ).splitlines()
    return [sha.strip("\\'") for sha in hashes]


def get_commit_metadata(sha: str) -> Dict:
    raw_output = (
        get_output(["git", "show", sha, "--format={}".format("%H%n%P%n%ct%n%B")])
        .strip("")
        .splitlines()
    )

    metadict = dict()
    metadict["parents"] = raw_output[1].split(" ")
    metadict["timestamp"] = int(raw_output[2])
    message = []
    for line in raw_output[2:]:
        if line.startswith("diff --git"):
            break
        message.append(line)
    metadict["message"] = message
    return metadict


def get_full_commits(depth: int, fix_indicator: Pattern = None) -> List[Commit]:
    """

    :param depth: the number of commits to show before the first
    :param fix_indicator: The regex used to identify fix commits
    :return: git show sha-depth separated into single commits, separated into
             lines
    """
    start = time()
    raw_commits = (
        get_output(
            [
                "git",
                "show",
                "-{}".format(str(depth)),
                # The format is hash \n parents \n date \n body
                "--format={}".format("linespots_commit_separator %H%n%P%n%ct%n%B"),
            ]
        )
        .strip("")
        .splitlines()
    )
    print("Raw commit gathering took {} seconds.".format(floor(time() - start)))
    nice_commits = []
    for commit_list in tqdm(
        get_chunks_by_line_start(raw_commits, "linespots_commit_separator"),
        desc="Getting Commits",
    ):
        if fix_indicator:
            nice_commits.append(Commit(commit_list, fix_indicator))
        else:
            nice_commits.append(Commit(commit_list))

    if raw_commits[0].startswith("linespots_commit_separator"):
        return nice_commits
    else:
        return nice_commits[1:]


def get_chunks_by_line_start(
    text: List[str], line_start: str, hunk_check: bool = False
) -> List[List[str]]:
    """
    Returns a list of code, seperated each time line_start is found as a line start.
    The code is held in a list of lines.
    """
    hunk_list = []
    current_list = []
    for line in text:
        if line.startswith(line_start):
            if hunk_check:
                if line.count("@") == 4:
                    if current_list:
                        hunk_list.append(current_list)
                        current_list = []
            else:
                if current_list:
                    hunk_list.append(current_list)
                    current_list = []

        current_list.append(line)

    hunk_list.append(current_list)
    return hunk_list


def is_fix_commit(commit_message: List[str], regex: Pattern) -> bool:
    # TODO get nice regex type
    """

    :param commit_message:
    :param regex:
    :return:
    """
    return any(regex.search(line) for line in commit_message)


def separate_body_diff(git_show: List[str]) -> Tuple[List[str], List[str]]:
    """
    >>> separate_body_diff(
    ... ['commit 9784e48bfb53030d189dff6cefc2ca2955ec987a',
    ... 'diff --git a/git_utils.py b/git_utils.py',
    ... 'index 1294062..1204048 100644'])
    (['commit 9784e48bfb53030d189dff6cefc2ca2955ec987a'], \
['diff --git a/git_utils.py b/git_utils.py', \
'index 1294062..1204048 100644'])

    """
    try:
        position = min(
            line_number
            for line_number, line in enumerate(git_show)
            if line.startswith("diff --git")
        )
    except ValueError:
        position = len(git_show)
    return git_show[:position], git_show[position:]


def get_diff_hunk_average_score(
    filescore_list: List[float], start: int, length: int
) -> float:
    """
    >>> get_diff_hunk_average_score([0,1,2,3,4,5,6,7,8,9,10], 1, 3)
    2.0

    :param filescore_list:
    :param start:
    :param length:
    :return:
    """
    if start == 0 and length == 0:
        return 0
    elif len(filescore_list[start : (length + start)]) == 0:
        return 0
    elif start >= 0 and length > 0:
        return sum(filescore_list[start : (length + start)]) / len(
            filescore_list[start : (length + start)]
        )

    else:
        logging.critical(
            "The start or length parameter did not make sense\n"
            "start: " + str(start) + " length: " + str(length) + "\n"
            "" + str(filescore_list)
        )
        raise sys.exit()


def get_hunk_metadata(meta_line: str) -> (int, int, int, int):
    """
    >>> get_hunk_metadata(
    ... "@@ -256,7 +257,8 @@ and even more text")
    (256, 7, 257, 8)

    :param meta_line:
    :return:
    """
    # possible regex to find the groups: "@@ ([-]?[\d]*[,]?[\d]*)?[ ]?([+]?[\d]*[,]?[\d]*)? @@"
    raw_values = meta_line[meta_line.find("-") : meta_line[2:].find("@@") + 1].strip()
    subtracted, added = tuple(
        value.strip("+- ").split(",") for value in raw_values.split(" ")
    )

    sub_start, sub_length = (
        subtracted if len(subtracted) == 2 else (0, subtracted.pop())
    )

    add_start, add_length = added if len(added) == 2 else (1, added.pop())

    return int(sub_start), int(sub_length), int(add_start), int(add_length)


def git_checkout_branch(branch: str):
    get_output(["git", "checkout", branch])


def git_checkout_revision_from_head(revision: int):
    get_output(["git", "checkout", "HEAD~{}".format(revision)])


def git_get_current_branch() -> str:
    return get_output(["git", "rev-parse", "--abbrev-ref", "HEAD"]).strip()


def get_project_line_count(project_files: Dict[str, List[float]]) -> int:
    project_lines = 0
    for file in project_files.items():
        project_lines += len(file[1])

    return project_lines


def wc_bundle_counter() -> Dict[str, List[int]]:
    tree = Popen(["git", "ls-tree", "-r", "--name-only", "HEAD"], stdout=PIPE)
    wc = Popen(["xargs", "wc", "-l"], stdin=tree.stdout, stdout=PIPE)
    tree.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
    output, err = wc.communicate()

    string_output = output.decode(encoding="UTF-8", errors="ignore")
    lines = [item.strip() for item in string_output.splitlines()][:-1]

    file_dict = dict()
    for line in lines:
        line_count, path = line.split(" ", 1)
        file_dict[path.strip()] = [0] * int(line_count)

    return file_dict


def python_count() -> Dict[str, List[int]]:
    git_paths = (
        get_output(["git", "ls-tree", "-r", "--name-only", "HEAD"])
        .strip("")
        .splitlines()
    )

    python_paths = []
    for root, dirs, files in os.walk("."):
        if not root.startswith("./.git"):
            for file in files:
                python_paths.append(os.path.join(root, file)[2:])

    missing_from_git = [path for path in python_paths if path not in git_paths]

    git_paths.extend(missing_from_git)
    bad_files = []
    file_dict = dict()
    for file in git_paths:
        if os.path.isfile(file):
            try:
                with open(file, "r", encoding="UTF-8", errors="strict") as f:
                    file_dict[file] = [0] * len(f.readlines())
            except UnicodeDecodeError:
                bad_files.append(file)

    for file in bad_files:
        with open(file, "r", encoding="ISO-8859-1", errors="strict") as f:
            file_dict[file] = [0] * len(f.readlines())

    return file_dict


def get_project_file_lines() -> Dict[str, List[int]]:
    wc_bundle_dict = wc_bundle_counter()
    python_dict = python_count()

    for key in python_dict.keys():
        if key in list(wc_bundle_dict.keys()):
            if len(wc_bundle_dict[key]) > len(python_dict[key]):
                python_dict[key] = python_dict[key].extend(
                    [0] * (len(wc_bundle_dict[key]) - len(python_dict[key]))
                )

    for key in wc_bundle_dict.keys():
        if key not in list(python_dict.keys()):
            python_dict[key] = wc_bundle_dict[key]

    return python_dict


def count_commits():
    return int(
        get_output(["git", "rev-list", "--count", "--first-parent", "HEAD"]).strip()
    )
