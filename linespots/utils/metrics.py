from typing import Dict, List
import pandas as pd
from numpy import trapz, argmax
from scipy.special import comb
from scipy.stats import wilcoxon
from math import floor
from collections import defaultdict
from tqdm import tqdm


def closed_density(score_df: pd.DataFrame, fixes_dict: Dict) -> Dict[str, List]:
    working_frame = score_df.sort_values(by="Score", ascending=False)

    fix_hashes = []
    for path in fixes_dict.keys():
        for sha, lines in fixes_dict[path]:
            if sha not in fix_hashes:
                fix_hashes.append(sha)
    fix_count = len(fix_hashes)

    if not fix_count:
        return {"fix_percentage": [1], "line_percentage": [1]}

    fix_percentage_list = []
    line_percentage_list = []
    # TODO this is a workaround for the fact that some lines either are not counted
    # properly or accidentally added during scoring.
    project_line_count = len(working_frame["Path"])

    for index, (path, line) in enumerate(
        zip(working_frame["Path"], working_frame["Line"])
    ):
        if path in fixes_dict.keys():
            for sha, lines in fixes_dict[path]:
                if line in lines:
                    if sha in fix_hashes:
                        fix_hashes.remove(sha)
                        line_percentage_list.append((index + 1) / project_line_count)
                        if fix_count > 0:
                            fix_percentage_list.append(
                                (fix_count - len(fix_hashes)) / fix_count
                            )
                        else:
                            fix_percentage_list.append(0)

    line_percentage_list.append(1)
    fix_percentage_list.append(1)

    return {
        "fix_percentage": fix_percentage_list,
        "line_percentage": line_percentage_list,
    }


def exam_list_fault_proportions(exam_list: List[float], fix_count: int) -> List[float]:
    fault_proportions = []
    for index, line_proportion in enumerate(exam_list):
        fault_proportions.append((1 / fix_count) * (index + 1))

    return fault_proportions


def aucec(line_percentage_list: List[float], fix_percentage_list: List[float]) -> float:
    if not line_percentage_list or not fix_percentage_list:
        return 0
    else:
        return trapz(y=fix_percentage_list + [1], x=line_percentage_list + [1])


def aucec_n(
    line_percentage_list: List[float], fix_percentage_list: List[float], n: float
) -> float:
    if not line_percentage_list or not fix_percentage_list:
        return 0
    else:
        cut_lines = [entry for entry in line_percentage_list if entry < n] + [n]
        cut_fixes = fix_percentage_list[: len(cut_lines)] + [
            fix_percentage_list[len(cut_lines) - 1]
        ]
        return trapz(y=cut_fixes, x=cut_fixes)


def e_inspect_n(exam_list: List[float], project_line_count: int, n: int):
    absolute_exam = [proportion * project_line_count for proportion in exam_list]
    found_fixes = 0
    for fix in absolute_exam:
        if fix <= n:
            found_fixes += 1
    return found_fixes


def exam_list_fix_count_fix_ranking(
    score_df: pd.DataFrame, fixes_dict: Dict
) -> (List[float], int, List[str]):
    project_line_count = len(score_df["Path"])
    working_frame = score_df.sort_values(by="Score", ascending=False)

    fix_hashes = []
    for path in fixes_dict.keys():
        for sha, lines in fixes_dict[path]:
            if sha not in fix_hashes:
                fix_hashes.append(sha)
    fix_count = len(fix_hashes)

    if not fix_count:
        return [], 0, []

    fix_ranking_list = []

    exam_list = []
    current_chunk = []
    for index, (path, line, score) in enumerate(
        tqdm(
            zip(working_frame["Path"], working_frame["Line"], working_frame["Score"]),
            total=len(working_frame["Path"]),
            desc="EXAM TOTAL:",
        )
    ):
        if not fix_hashes:
            break
        elif (
            not current_chunk
            or score == current_chunk[-1][3]
            and (index + 1) < len(working_frame["Path"])
        ):
            current_chunk.append([index, path, line, score])
        else:
            t_f_dict = defaultdict(int)
            for e_index, e_path, e_line, e_score in current_chunk:
                if e_path in fixes_dict.keys():
                    for sha, lines in fixes_dict[e_path]:
                        if sha in fix_hashes and e_line in lines:
                            t_f_dict[sha] += 1
            if t_f_dict:
                p_start = current_chunk[0][0] + 1
                t = len(current_chunk)
                for entry in t_f_dict.keys():
                    t_f = t_f_dict[entry]
                    if t_f == 1:
                        exam_list.append((p_start + ((t - 1) / 2)) / project_line_count)
                        fix_ranking_list.append(
                            (entry, ((p_start + ((t - 1) / 2)) / project_line_count))
                        )
                    elif t_f == t:
                        exam_list.append(p_start / project_line_count)
                        fix_ranking_list.append((entry, (p_start / project_line_count)))
                    else:
                        # This could probably be parallalelized for performance improvements.
                        sum_term = sum(
                            [
                                k
                                * (
                                    comb(t - k - 1, t_f - 1, exact=True)
                                    / comb(t, t_f, exact=True)
                                )
                                for k in range(1, t - t_f + 1)
                            ]
                        )
                        exam_list.append((p_start + sum_term) / project_line_count)
                        fix_ranking_list.append(
                            (entry, ((p_start + sum_term) / project_line_count))
                        )
                        del sum_term

                    fix_hashes.remove(entry)
            current_chunk = list()
            current_chunk.append([index, path, line, score])

    return (
        sorted(exam_list),
        fix_count,
        [sha for sha, lines in sorted(fix_ranking_list, key=lambda tup: tup[1])],
    )


def confusion_matrix_at_n(
    score_df: pd.DataFrame, fixes_dict: Dict, cut_off_point: float
) -> Dict[str, int]:
    working_frame = score_df.sort_values(by="Score", ascending=False)
    n = floor(len(working_frame["Path"]) * cut_off_point)

    fix_hashes = []
    for path in fixes_dict:
        for sha, lines in fixes_dict[path]:
            if sha not in fix_hashes:
                fix_hashes.append(sha)
    if not len(fix_hashes):
        line_based_dict = dict()
        line_based_dict["tp"] = 0
        line_based_dict["tn"] = len(working_frame["Path"]) - n
        line_based_dict["fp"] = n
        line_based_dict["fn"] = 0
        return line_based_dict

    true_positive = 0
    false_positive = 0
    for path, line in zip(working_frame["Path"][:n], working_frame["Line"][:n]):
        if path in fixes_dict.keys():
            hit = False
            for sha, lines in fixes_dict[path]:
                if line in lines:
                    true_positive += 1
                    hit = True
                    break
            if not hit:
                false_positive += 1
        else:
            false_positive += 1

    true_negative = 0
    false_negative = 0
    for path, line in zip(working_frame["Path"][n:], working_frame["Line"][n:]):
        if path in fixes_dict.keys():
            hit = False
            for sha, lines in fixes_dict[path]:
                if line in lines:
                    false_negative += 1
                    hit = True
                    break
            if not hit:
                true_negative += 1
        else:
            true_negative += 1

    assert len(working_frame["Path"]) == (
        true_positive + true_negative + false_positive + false_negative
    )

    line_based_dict = dict()
    line_based_dict["tp"] = true_positive
    line_based_dict["tn"] = true_negative
    line_based_dict["fp"] = false_positive
    line_based_dict["fn"] = false_negative
    return line_based_dict


def best_hit_density_from_exam(
    line_percentage_list: List[float], fix_percentage_list: List[float]
) -> (float, float):
    if not line_percentage_list or not fix_percentage_list:
        return 0, 1

    density_list = []
    for line_proportion, fix_proportion in zip(
        line_percentage_list, fix_percentage_list
    ):
        density_list.append(fix_proportion / line_proportion)

    return max(density_list), line_percentage_list[int(argmax(density_list))]


def best_hit_density_from_density(
    line_percentage_list: List[float], fix_percentage_list: List[float]
) -> (float, float):
    if not line_percentage_list or not fix_percentage_list:
        return 0, 1
    density_list = [
        fixes / lines for lines, fixes in zip(line_percentage_list, fix_percentage_list)
    ]
    return max(density_list), (argmax(density_list) + 1) / len(density_list)


def rank_differences(
    linespots_ranking: List[str], bugspots_ranking: List[str]
) -> (int, float, (float, float)):
    if not linespots_ranking or not bugspots_ranking:
        return 0, 0, (1, 0)

    linespots_dict = dict()
    for index, sha in enumerate(linespots_ranking):
        linespots_dict[sha] = index + 1

    bugspots_dict = dict()
    for index, sha in enumerate(bugspots_ranking):
        bugspots_dict[sha] = index + 1

    difference_list = [
        linespots_dict[sha] - bugspots_dict[sha] for sha in linespots_ranking
    ]
    sum_of_absolute_error = sum([abs(difference) for difference in difference_list])

    if any(difference_list):
        wilcox = wilcoxon(difference_list)
    else:
        wilcox = [1, 0]

    return (
        sum_of_absolute_error,
        sum_of_absolute_error / len(linespots_ranking),
        wilcox,
    )
