from typing import Dict, List, DefaultDict
import pandas as pd
from numpy import average
from math import ceil

import linespots.utils.metrics as met


def extend_linespots_dict(
    linespots_scores_dict: Dict[str, List[float]],
    project_lines_dict: Dict[str, List[float]],
) -> Dict[str, List[float]]:
    extended_dict = {}
    ls_keys = list(linespots_scores_dict.keys())
    pr_keys = list(project_lines_dict.keys())

    for key in ls_keys:
        extended_dict[key] = linespots_scores_dict[key]
        if key in pr_keys:
            if len(project_lines_dict[key]) > len(linespots_scores_dict[key]):
                extended_dict[key].extend(
                    [0]
                    * (len(project_lines_dict[key]) - len(linespots_scores_dict[key]))
                )
            pr_keys.remove(key)

    for key in pr_keys:
        extended_dict[key] = project_lines_dict[key]
    return extended_dict


def extend_bugspots_dict(
    bugspots_scores_dict: Dict[str, float],
    linespots_scores_dict: Dict[str, List[float]],
) -> Dict[str, List[float]]:
    extended_dict = {}
    ls_keys = list(linespots_scores_dict.keys())
    bs_keys = list(bugspots_scores_dict.keys())
    for key in bs_keys:
        if key in ls_keys:
            extended_dict[key] = [bugspots_scores_dict[key]] * len(
                linespots_scores_dict[key]
            )
            ls_keys.remove(key)

    for key in ls_keys:
        extended_dict[key] = [0] * len(linespots_scores_dict[key])

    return extended_dict


def run_result_df(
    score_df: pd.DataFrame,
    fixes_dict: Dict,
    project: str,
    weight: str,
    time: str,
    domain: str,
) -> pd.DataFrame:
    paths = []
    lines = []
    scores = []
    faultys = []
    shas = []

    for path, line, score in zip(score_df["Path"], score_df["Line"], score_df["Score"]):
        paths.append(path)
        lines.append(line)
        scores.append(score)

        hit = False
        if path in fixes_dict.keys():
            for sha, numbers in fixes_dict[path]:
                if line in numbers:
                    faultys.append(1)
                    shas.append(sha)
                    hit = True
                    break
        if not hit:
            faultys.append(0)
            shas.append(0)
    assert len(paths) == len(lines) == len(scores) == len(faultys) == len(shas)

    return pd.DataFrame(
        {
            "Project": [project] * len(paths),
            "Weight": [weight] * len(paths),
            "Time": [time] * len(paths),
            "Domain": [domain] * len(paths),
            "Path": paths,
            "Line": lines,
            "Score": scores,
            "Faulty": faultys,
            "Sha": shas,
        }
    ).sort_values(["Path", "Line"])


def future_fault_dict_to_dataframe(future_dict: DefaultDict[str, List]) -> pd.DataFrame:
    keys = []
    lines = []
    hashes = []
    for key in future_dict.keys():
        for sha, new_lines in future_dict[key]:
            lines.extend(new_lines)
            hashes.extend([sha] * len(new_lines))
            keys.extend([key] * len(new_lines))

    return pd.DataFrame({"Path": keys, "Line": lines, "Sha": hashes}).sort_values(
        ["Path", "Line"]
    )


def scores_dict_to_df(
    scores_dict: Dict[str, List], standardize: bool = True
) -> pd.DataFrame:
    paths = []
    lines = []
    scores = []
    for key in scores_dict.keys():
        for line, score in enumerate(scores_dict[key]):
            lines.append(line + 1)
            scores.append(score)
            paths.append(key)
    if not standardize:
        return pd.DataFrame(
            {"Path": paths, "Line": lines, "Score": scores}
        ).sort_values(["Score", "Path", "Line"])
    else:
        frame = pd.DataFrame(
            {"Path": paths, "Line": lines, "Score": scores}
        ).sort_values(["Score", "Path", "Line"])
        series = frame.loc[:, "Score"]
        avg = series.mean()
        stdv = series.std()
        frame["Score"] = (series - avg) / stdv
        return frame


def scores_and_faults_to_result_dict(
    future_fixes_dict: Dict,
    scores_df: pd.DataFrame,
    project: str,
    domain: str,
    weighting_function: str,
    time_version: str,
    depth: int,
    future_size: int,
    origin: int,
    project_commit_count: int,
    algorithm: str,
    run_id: int,
    source: str,
    choice: str,
    language: str,
) -> Dict:

    exam_list, fix_count, fix_ranking = met.exam_list_fix_count_fix_ranking(
        score_df=scores_df, fixes_dict=future_fixes_dict
    )

    density_dict = met.closed_density(score_df=scores_df, fixes_dict=future_fixes_dict)

    fault_proportions = met.exam_list_fault_proportions(exam_list, fix_count)

    confusion_matrix = met.confusion_matrix_at_n(
        score_df=scores_df, fixes_dict=future_fixes_dict, cut_off_point=0.05
    )

    hd_max_exam, hd_max_loc_exam = met.best_hit_density_from_exam(
        line_percentage_list=exam_list, fix_percentage_list=fault_proportions
    )

    hd_max_density, hd_max_loc_density = met.best_hit_density_from_density(
        line_percentage_list=density_dict["line_percentage"],
        fix_percentage_list=density_dict["fix_percentage"],
    )
    if not exam_list:
        exam_list = [1]
    return {
        "Project": project,
        "Domain": domain,
        "Weighting": weighting_function,
        "Time": time_version,
        "Origin": origin,
        "Future": future_size,
        "Depth": depth,
        "AUCECEXAM": met.aucec(
            line_percentage_list=exam_list, fix_percentage_list=fault_proportions
        ),
        "AUCECDENSITY": met.aucec(
            line_percentage_list=density_dict["line_percentage"],
            fix_percentage_list=density_dict["fix_percentage"],
        ),
        "AUCECEXAM5": met.aucec_n(
            line_percentage_list=exam_list,
            fix_percentage_list=fault_proportions,
            n=0.05,
        ),
        "AUCECDENSITY5": met.aucec_n(
            line_percentage_list=density_dict["line_percentage"],
            fix_percentage_list=density_dict["fix_percentage"],
            n=0.05,
        ),
        "EInspect10EXAM": met.e_inspect_n(exam_list, len(scores_df["Path"]), 10),
        "EInspect25EXAM": met.e_inspect_n(exam_list, len(scores_df["Path"]), 25),
        "EInspect10Density": met.e_inspect_n(
            density_dict["fix_percentage"], len(scores_df["Path"]), 10
        ),
        "EInspect25Density": met.e_inspect_n(
            density_dict["fix_percentage"], len(scores_df["Path"]), 25
        ),
        "EXAM": average(exam_list),
        "EXAM95": average(exam_list[: ceil(len(exam_list) / 1.05)]),
        "EXAM75": average(exam_list[: ceil(len(exam_list) / 1.33)]),
        "EXAM50": average(exam_list[: ceil(len(exam_list) / 2)]),
        "EXAM33": average(exam_list[: ceil(len(exam_list) / 3)]),
        "EXAM25": average(exam_list[: ceil(len(exam_list) / 4)]),
        "EXAMF": exam_list[0],
        "FullExam": exam_list,
        "tp": confusion_matrix["tp"],
        "fp": confusion_matrix["fp"],
        "tn": confusion_matrix["tn"],
        "fn": confusion_matrix["fn"],
        "hdMaxEXAM": hd_max_exam,
        "hdMaxLOCEXAM": hd_max_loc_exam,
        "hdMaxDensity": hd_max_density,
        "hdMaxLOCDensity": hd_max_loc_density,
        "FixCount": fix_count,
        "LOC": len(scores_df["Path"]),
        "Commits": project_commit_count,
        "Algorithm": algorithm,
        "ID": run_id,
        "Source": source,
        "Choice": choice,
        "Language": language,
        "FixRanks": fix_ranking,
    }
