import json
import os
import linespots.utils.git_utils as git
from math import floor
from random import randint, seed


def distanced_random_int(n: int, min_int: int, max_int: int, distance: int):
    if n > 1:
        while True:
            numbers = sorted([randint(min_int, max_int) for i in range(n)])
            test = [0] * (len(numbers) - 1)
            for i in range(len(numbers) - 1):
                if numbers[i + 1] - numbers[i] < distance:
                    test[i] = 1
            if not any(test):
                break
        return numbers
    else:
        return [randint(min_int, max_int)]


def run():
    seed(140919)

    project_names = [
        "prestashop",
        "scikit-learn",
        "broadleafcommerce",
        "ceylon-ide-eclipse",
        "woocommerce",
        "ffmpeg",
        "rails",
        "lucene-solr",
        "server",
        "mysql-server",
        "mpc-hc",
        "junit5",
        "rt.equinox.framework",
        "bootstrap",
        "cpython",
        "discourse",
        "mongoose",
        "commons-math",
        "closure-compiler",
        "jfreechart",
        "coala",
        "evolution",
        "httpd",
    ]

    project_urls = [
        "https://github.com/PrestaShop/prestashop.git",
        "https://github.com/scikit-learn/scikit-learn.git",
        "https://github.com/BroadleafCommerce/broadleafcommerce.git",
        "https://github.com/eclipse/ceylon-ide-eclipse.git",
        "https://github.com/woocommerce/woocommerce.git",
        "https://github.com/FFmpeg/ffmpeg.git",
        "https://github.com/rails/rails.git",
        "https://github.com/apache/lucene-solr.git",
        "https://github.com/MariaDB/server.git",
        "https://github.com/mysql/mysql-server.git",
        "https://github.com/mpc-hc/mpc-hc.git",
        "https://github.com/junit-team/junit5.git",
        "https://github.com/eclipse/rt.equinox.framework.git",
        "https://github.com/twbs/bootstrap.git",
        "https://github.com/python/cpython.git",
        "https://github.com/discourse/discourse",
        "https://github.com/Automattic/mongoose",
        "https://github.com/apache/commons-math.git",
        "https://github.com/google/closure-compiler.git",
        "https://github.com/jfree/jfreechart.git",
        "https://github.com/coala/coala.git",
        "https://github.com/GNOME/evolution.git",
        "https://github.com/apache/httpd.git",
    ]

    project_domains = [
        "25502020",
        "45102020",
        "25502020",
        "45103010",
        "25502020",
        "50202010",
        "45102030",
        "45102020",
        "45102020",
        "45102020",
        "50202010",
        "45103020",
        "45103020",
        "45102030",
        "45103020",
        "45103010",
        "45102020",
        "45103020",
        "45103020",
        "45103020",
        "45103020",
        "45103010",
        "45102030",
    ]

    project_fix_indicators = [
        "fix |fixes ",
        "fix |fixes ",
        "fix |fixes |fixed",
        "fix |fixed ",
        "fix |fixes ",
        "fix |fixed ",
        "fix |fixes ",
        "fix ",
        "fix |fixed |fixing ",
        "fix ",
        "fix ",
        "fixes ",
        "fix ",
        "fix ",
        "fix |fixes ",
        "FIX:|fix ",
        "fix:|fix",
        "fix|fixed ",
        "fix |fixed ",
        "fix |fixed ",
        "Fixes",
        "fix |fixes ",
        "fix ",
    ]

    sources = [
        "Author",
        "Author",
        "Author",
        "Past",
        "Author",
        "Random",
        "Author",
        "Past",
        "Author",
        "Author",
        "Author",
        "Past",
        "Past",
        "Author",
        "Author",
        "Random",
        "Random",
        "Past",
        "Past",
        "Past",
        "Past",
        "Past",
        "Random",
    ]

    choices = [
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Random",
        "Author",
        "Author",
        "Author",
        "Author",
        "Author",
        "Author",
        "Author",
        "Author",
    ]

    languages = [
        "PHP",
        "Python",
        "Java",
        "Java",
        "PHP",
        "C",
        "Ruby",
        "Java",
        "C++",
        "C++",
        "C",
        "Java",
        "Java",
        "JS",
        "Python",
        "Ruby",
        "JS",
        "Java",
        "Java",
        "Java",
        "Python",
        "C",
        "C",
    ]

    os.chdir("../../../evaluation_projects")

    json_dict = {}

    for index, name in enumerate(project_names):
        os.chdir(name)

        json_dict[name] = {}
        json_dict[name]["project_name"] = name
        json_dict[name]["domain"] = project_domains[index]
        json_dict[name]["fix_indicator"] = project_fix_indicators[index]
        json_dict[name]["source"] = sources[index]
        json_dict[name]["choice"] = choices[index]
        json_dict[name]["language"] = languages[index]

        commit_count = git.count_commits()
        json_dict[name]["commits"] = commit_count
        depth = 2000
        future = 1000
        possible_sample_count = floor(commit_count / (depth + future))
        try:
            json_dict[name]["origins"] = distanced_random_int(
                min(4, possible_sample_count),
                depth,
                (commit_count - future),
                depth + future,
            )

        except:
            print("hi")
        json_dict[name]["depths"] = [depth] * len(json_dict[name]["origins"])
        json_dict[name]["futures"] = [future] * len(json_dict[name]["origins"])

        os.chdir("..")

    with open("final-config.json", "w") as f:
        json.dump(json_dict, f)


if __name__ == "__main__":
    run()
