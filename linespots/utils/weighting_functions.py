from math import exp


def normalize_timestamp_to_commit_range(
    current_commit_timestamp: int,
    oldest_commit_timestamp: int,
    youngest_commit_timestamp: int,
) -> float:
    # Sanitize the input
    assert oldest_commit_timestamp < youngest_commit_timestamp
    # This is a workaround for commits that are younger than the youngest timestamp due to rewritten time.
    if current_commit_timestamp > youngest_commit_timestamp:
        return 1
    # This is a workaround for commits that are older than the oldest timestamp due to rewritten time.
    if current_commit_timestamp < oldest_commit_timestamp:
        return 0

    # In case any of the numbers is negative or anything else goes wrong, return between 0 and 1.
    return min(
        1.0,
        max(
            0.0,
            (current_commit_timestamp - oldest_commit_timestamp)
            / (youngest_commit_timestamp - oldest_commit_timestamp),
        ),
    )


def google_weighting_function(
    commit_age: float, old_hunk_average_score: float
) -> float:
    return old_hunk_average_score + (1 / (1 + exp((-12 * commit_age) + 12)))


def linear_weighting_function(
    commit_age: float, old_hunk_average_score: float
) -> float:
    return old_hunk_average_score + commit_age


def flat_weighting_function(commit_age: float, old_hunk_average_score: float) -> float:
    # This is essentially Rahman's initial algorithm (applied to line level through Linespots)
    return old_hunk_average_score + 1
