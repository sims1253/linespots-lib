from copy import deepcopy
from collections import defaultdict
from typing import Dict, List, DefaultDict

import linespots.utils.git_utils as git


def get_future_commit_faulty_lines_and_line_updates(
    files_line_numbers: Dict[str, List[int]], commit: git.Commit
) -> (Dict[str, List[int]], Dict[str, List[List]]):
    faulty_lines = defaultdict(list)
    # Each file in the diff is scored according to the file mode (new,
    # delete, rename or modify)
    for file_diff in commit.file_diffs:
        if file_diff.file_mode == "delete":
            try:
                del files_line_numbers[file_diff.filename]
            except KeyError:
                pass

        elif (
            file_diff.is_binary
            or file_diff.is_mode_change
            or file_diff.file_mode == "new"
        ):
            pass
        elif file_diff.file_mode == "rename" and file_diff.similarity_index == 100:
            if file_diff.old_filename in files_line_numbers.keys():
                files_line_numbers[file_diff.filename] = files_line_numbers.pop(
                    file_diff.old_filename
                )
        else:
            # If this is False, the file was created in the future so we can ignore it
            if file_diff.filename in files_line_numbers.keys():
                new_file_lines, file_faulty_lines = get_future_file_faulty_lines_and_line_updates(
                    file_diff=file_diff,
                    old_file_line_numbers=files_line_numbers[file_diff.filename],
                    fix=commit.is_fix,
                )
                files_line_numbers[file_diff.filename] = new_file_lines
                if commit.is_fix and file_faulty_lines:
                    faulty_lines[file_diff.filename] = [commit.sha, file_faulty_lines]

    return files_line_numbers, faulty_lines


def get_future_file_faulty_lines_and_line_updates(
    file_diff: git.FileDiff, old_file_line_numbers: List[int], fix: bool
) -> (List[int], List[int]):
    new_line_scores = []
    faulty_lines = []
    # While new files start with 0 in the diff, there is no old file score list to extend
    # from anyway.
    current_old_line_number = 0
    for hunk in file_diff.hunks:

        # Fills up the new scores list up to the current hunk's start
        if hunk.old_start > current_old_line_number:
            extension = old_file_line_numbers[
                current_old_line_number : hunk.old_start - 1
            ]

            # TODO find fix instead of workaround for
            # raise AssertionError(
            #     "There should not be missing past parts during evaluation"
            # )
            if len(extension) < (hunk.old_start - current_old_line_number - 1):
                extension.extend(
                    [-1]
                    * ((hunk.old_start - current_old_line_number - 1) - len(extension))
                )

            new_line_scores.extend(extension)
            current_old_line_number = hunk.old_start - 1

        # Add the scores for the current hunk to the new scores list
        new_hunk_lines, hunk_faulty_lines = get_future_hunk_faulty_lines_and_line_updates(
            hunk=hunk, old_line_numbers=old_file_line_numbers, fix=fix
        )
        new_line_scores.extend(new_hunk_lines)
        if fix:
            faulty_lines.extend(hunk_faulty_lines)
        # Keep track of where we are on the old scores list
        current_old_line_number += hunk.old_length

    # If the old score list wasn't completely traversed by the hunks, the
    # missing lines are appended at the end of the new list unchanged.
    if current_old_line_number < len(old_file_line_numbers):
        new_line_scores.extend(old_file_line_numbers[current_old_line_number:])

    return new_line_scores, faulty_lines


def get_future_hunk_faulty_lines_and_line_updates(
    hunk: git.HunkDiff, old_line_numbers: List[int], fix: bool
) -> (List[int], List[int]):

    new_line_scores = []
    faulty_line_numbers = []
    # Have to subtract 1 to compensate for 0 indexed list vs 1 indexed line numbers in git
    current_old_line_number = hunk.old_start

    for line in hunk.content:
        # '-' means that the line was modified. If it is a fix,
        # we interpret the line as faulty.
        if line.startswith("-"):
            # If line number is -1 than it is a line from the future and could not
            # have been predicted so we ignore it.
            try:
                if fix and old_line_numbers[current_old_line_number - 1] > 0:
                    faulty_line_numbers.append(
                        old_line_numbers[current_old_line_number - 1]
                    )
            except:
                # TODO This should be either caused by bad diff creation or line counting
                # Fix this properly. Now we just assume it was a new line that was faulty.
                pass
            current_old_line_number += 1
        # '+' adds a new score
        elif line.startswith("+"):
            # Add -1 to indicate this is a line that was introduced in the future.
            new_line_scores.append(-1)
        else:
            # Ignore this as it is not part of the file but git output
            if line == "\ No newline at end of file":
                pass
            # Add the unmodified line from the old to the new line tracker
            # TODO this try except should not be necessary. Maybe it's the encoding bug.q
            else:
                try:
                    new_line_scores.append(
                        old_line_numbers[current_old_line_number - 1]
                    )
                    current_old_line_number += 1
                except IndexError:
                    new_line_scores.append(-1)
                    current_old_line_number += 1
    return new_line_scores, faulty_line_numbers


def get_future_faulty_lines_dict(
    commits: List[git.Commit], project_lines: Dict[str, List[int]]
) -> DefaultDict[str, List]:
    """

    :param commits:  List of commits to analyze
    :param fix_indicator: regex to identify fault fixing commit messages
    :param project_lines:
    :return: dict of files at point origin with all lines that contain faults marked
    """
    branch = git.git_get_current_branch()
    files_dict = deepcopy(project_lines)
    # Fill file lists with line numbers for identification of moved lines
    # after future changes
    for path in files_dict.keys():
        for i in range(len(files_dict[path])):
            files_dict[path][i] = i + 1

    faulty_lines_dict = defaultdict(list)
    for commit in reversed(commits):
        new_file_line_numbers, commit_faulty_lines = get_future_commit_faulty_lines_and_line_updates(
            files_line_numbers=files_dict, commit=commit
        )
        files_dict = new_file_line_numbers

        for file in commit_faulty_lines.keys():
            faulty_lines_dict[file].append(commit_faulty_lines[file])

    return faulty_lines_dict
