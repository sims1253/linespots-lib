import os
import pandas as pd
from tqdm import tqdm
from typing import List, Callable, Pattern
import linespots.bugspots as bs
from linespots.evaluation.future_fault_finder import get_future_faulty_lines_dict
import linespots.linespots as ls
import linespots.utils.git_utils as git
import linespots.utils.metrics as met
import linespots.utils.transformations as ts


def full_project_evaluation(
    project_path: str,
    domain: str,
    weighting_functions: List[Callable[[float, float], float]],
    time_versions: List[str],
    origins: List[int],
    depths: List[int],
    futures: List[int],
    fix_indicator: Pattern,
    source: str,
    choice: str,
    language: str,
):
    assert len(origins) == len(depths) == len(futures)
    # Origin must be bigger than Future or a commit might be lost
    assert not any([future >= origin for future, origin in zip(futures, origins)])

    origin_path = os.getcwd()
    os.chdir(project_path)
    result_dict_list = []
    branch = git.git_get_current_branch()
    project_commit_count = git.count_commits()
    sha_list = git.get_git_log(
        max(
            [
                project_commit_count - (origin - depth)
                for origin, depth in zip(origins, depths)
            ]
        )
    )

    run_id = 1
    for origin, future, depth in zip(origins, futures, depths):

        past_commits = [
            git.Commit(sha, fix_indicator)
            for sha in tqdm(
                sha_list[
                    project_commit_count
                    - origin : project_commit_count
                    - (origin - depth)
                ],
                desc="Building past commits!",
            )
        ]

        future_commits = [
            git.Commit(sha, fix_indicator)
            for sha in tqdm(
                sha_list[
                    project_commit_count
                    - (origin + future) : project_commit_count
                    - origin
                ],
                desc="Building future commits!",
            )
        ]
        git.git_checkout_revision_from_head(project_commit_count - origin)
        project_lines = git.get_project_file_lines()
        git.git_checkout_branch(branch)

        future_faults_dict = get_future_faulty_lines_dict(
            commits=future_commits, project_lines=project_lines
        )

        for weight in weighting_functions:
            for time_v in time_versions:
                linespots_scores_dict = ls.get_line_scores(
                    commits=past_commits, weighting_function=weight, time_version=time_v
                )
                linespots_scores_dict = ts.extend_linespots_dict(
                    linespots_scores_dict, project_lines
                )
                linespots_scores_df = ts.scores_dict_to_df(linespots_scores_dict)

                assert git.get_project_line_count(
                    linespots_scores_dict
                ) >= git.get_project_line_count(project_lines)

                bugspots_score_dict = bs.get_bugspots(
                    commits=past_commits, weighting_function=weight, time_version=time_v
                )
                bugspots_score_dict = ts.extend_bugspots_dict(
                    bugspots_score_dict, linespots_scores_dict
                )
                bugspots_scores_df = ts.scores_dict_to_df(bugspots_score_dict)

                assert git.get_project_line_count(
                    bugspots_score_dict
                ) >= git.get_project_line_count(project_lines)

                # Make sure that only fixes are in the dict that we could predict
                delete_path_list = []
                delete_sha_list = []
                for path in future_faults_dict.keys():
                    if (
                        path not in linespots_scores_dict.keys()
                        or path not in bugspots_score_dict.keys()
                    ):
                        delete_path_list.append(path)
                    else:
                        for sha, lines in future_faults_dict[path]:
                            if not len(linespots_scores_dict[path]) >= max(lines):
                                delete_sha_list.append([path, sha])
                for path in delete_path_list:
                    del future_faults_dict[path]
                for path, sha in delete_sha_list:
                    del future_faults_dict[path][sha]

                result_dict_list.append(
                    ts.scores_and_faults_to_result_dict(
                        future_fixes_dict=future_faults_dict,
                        scores_df=linespots_scores_df,
                        project=project_path.split("/")[-1],
                        domain=domain,
                        weighting_function=weight.__name__,
                        time_version=time_v,
                        depth=depth,
                        future_size=future,
                        origin=origin,
                        project_commit_count=project_commit_count,
                        algorithm="Linespots",
                        run_id=run_id,
                        source=source,
                        choice=choice,
                        language=language,
                    )
                )

                result_dict_list.append(
                    ts.scores_and_faults_to_result_dict(
                        future_fixes_dict=future_faults_dict,
                        scores_df=bugspots_scores_df,
                        project=project_path.split("/")[-1],
                        domain=domain,
                        weighting_function=weight.__name__,
                        time_version=time_v,
                        depth=depth,
                        future_size=future,
                        origin=origin,
                        project_commit_count=project_commit_count,
                        algorithm="Bugspots",
                        run_id=run_id,
                        source=source,
                        choice=choice,
                        language=language,
                    )
                )
                run_id += 1
                sum_rank_difference, mean_rank_difference, (
                    w,
                    p,
                ) = met.rank_differences(
                    linespots_ranking=result_dict_list[-2]["FixRanks"],
                    bugspots_ranking=result_dict_list[-1]["FixRanks"],
                )
                result_dict_list[-1]["SRD"] = sum_rank_difference
                result_dict_list[-2]["SRD"] = sum_rank_difference
                result_dict_list[-1]["MRD"] = mean_rank_difference
                result_dict_list[-2]["MRD"] = mean_rank_difference
                result_dict_list[-1]["WCP"] = p
                result_dict_list[-2]["WCP"] = p
                result_dict_list[-1]["WCW"] = w
                result_dict_list[-2]["WCW"] = w

        git.git_checkout_branch(branch)

    git.git_checkout_branch(branch)
    os.chdir(origin_path)
    return pd.DataFrame(result_dict_list)
