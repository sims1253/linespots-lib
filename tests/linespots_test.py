import pytest

import linespots.utils.git_utils as git
from linespots.linespots import (
    normalize_timestamp_to_commit_range,
    score_hunk_diff,
    score_file_diff,
)


class Test_normalize_timestamp_to_commit_range:
    def test_switched_old_and_new(self):
        with pytest.raises(AssertionError) as normalize_exception:
            normalize_timestamp_to_commit_range(1511820669, 1511820690, 1511820650)

    def test_current_out_of_range_top(self):
        with pytest.raises(AssertionError) as normalize_exception:
            normalize_timestamp_to_commit_range(1511820690, 1511820650, 1511820680)

    def test_current_out_of_range_bottom(self):
        with pytest.raises(AssertionError) as normalize_exception:
            normalize_timestamp_to_commit_range(1511820640, 1511820650, 1511820680)

    def test_negative_timestamp(self):
        with pytest.raises(AssertionError) as normalize_exception:
            normalize_timestamp_to_commit_range(15, -5, 30)

    def test_proper_working(self):
        result = normalize_timestamp_to_commit_range(50, 0, 100)
        assert result == 0.5

    def test_current_equals_oldest(self):
        result = normalize_timestamp_to_commit_range(0, 0, 100)
        assert result == 0

    def test_current_equals_youngest(self):
        result = normalize_timestamp_to_commit_range(100, 0, 100)
        assert result == 1


class Test_score_hunk_diff:
    def test_hunk_scoring(self):
        result = []
        score_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
        with open("tests/score_hunk_test_data1.txt") as f:
            hunks = f.readlines()

        for index, hunk in enumerate(git.get_chunks_by_line_start(hunks, "@@")):
            result.append(score_hunk_diff(hunk, 1, score_list, True))

        assert result[0] == [7, 8, 9, 9, 10]
        assert result[1] == [14, 16.5, 16.5, 16.5, 16.5, 16, 16]


class Test_score_file_diff:
    def test_file_scoring(self):
        score_list = [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
        ]
        with open("tests/score_hunk_test_data1.txt") as f:
            hunks = f.readlines()

        diff = git.get_chunks_by_line_start(hunks, "@@")
        result = score_file_diff(diff, 1, score_list, True)

        assert result == [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            9,
            10,
            11,
            12,
            13,
            14,
            17,
            17,
            17,
            17,
            19,
            20,
            20,
        ]
