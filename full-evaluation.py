import json
import os
import re
from collections import defaultdict
from multiprocessing import Pool
import pandas as pd
from linespots.evaluation.evaluate import full_project_evaluation
from linespots.utils.weighting_functions import (
    google_weighting_function,
    flat_weighting_function,
    linear_weighting_function,
)


def map_call_evaluation(project):
    print("Starting Project {}".format(project["project_name"]))
    result = full_project_evaluation(
        project_path=project["project_name"],
        domain=project["domain"],
        weighting_functions=[
            google_weighting_function,
            linear_weighting_function,
            flat_weighting_function,
        ],
        time_versions=["time", "commit"],
        origins=project["origins"],
        depths=project["depths"],
        futures=project["futures"],
        fix_indicator=re.compile(project["fix_indicator"], re.IGNORECASE),
        source=project["source"],
        choice=project["choice"],
        language=project["language"],
    )

    pd.DataFrame(result).to_csv(
        "{}-evaluation.csv".format(project["project_name"]), index=False
    )
    print("Finished Project {}".format(project["project_name"]))
    return result


os.chdir("../evaluation_projects")

with open("missing.json", "r") as file:
    config = json.loads(file.read())

config_list = [config[key] for key in config.keys()]
result_list = []
project_iter = []
for project in config_list:
    project_iter.append(project)
    if len(project_iter) == 2:
        with Pool(2) as p:
            result_list.extend(p.imap_unordered(map_call_evaluation, project_iter))
        project_iter = []
if project_iter:
    with Pool(1) as p:
        result_list.extend(p.imap_unordered(map_call_evaluation, project_iter))
result_dict = defaultdict(list)
for result in result_list:
    for column in result.keys():
        result_dict[column].extend(result[column])
pd.DataFrame(result_dict).to_csv("full_evaluation.csv", index=False)
